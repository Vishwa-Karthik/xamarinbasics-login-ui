## Simple UI Login form
### Designed simple login UI using C# 

## Results
<p>
<img src="https://gitlab.com/Vishwa-Karthik/xamarinbasics-login-ui/-/raw/master/outputs/Login1.png" width="200" height="400">
<img src="https://gitlab.com/Vishwa-Karthik/xamarinbasics-login-ui/-/raw/master/outputs/Login2.png" width="200" height="400">
<img src="https://gitlab.com/Vishwa-Karthik/xamarinbasics-login-ui/-/raw/master/outputs/Login3.png" width="200" height="400">

</p>
